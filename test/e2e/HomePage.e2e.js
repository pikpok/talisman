import { ClientFunction } from 'testcafe';
import { ReactSelector, waitForReact } from 'testcafe-react-selectors';
import { getPageUrl } from './helpers';

const getPageTitle = ClientFunction(() => document.title);
const assertNoConsoleErrors = async t => {
  const { error } = await t.getBrowserConsoleMessages();
  await t.expect(error).eql([]);
};

fixture`Home Page`.page('../../app/app.html').afterEach(assertNoConsoleErrors);

test('e2e', async t => {
  await t.expect(getPageTitle()).eql('Talisman');
});

test('should open window', async t => {
  await t.expect(getPageTitle()).eql('Talisman');
});

test(
  "shouldn't have any logs in console of main window",
  assertNoConsoleErrors
);

test('should navgiate to /lobby and back', async t => {
  await waitForReact();
  await t
    .click(
      ReactSelector('Link').withProps({
        to: '/lobby'
      })
    )
    .expect(getPageUrl())
    .contains('/lobby')
    .click('[data-tid="backButton"]')
    .expect(getPageUrl())
    .match(/#\/$/);
});
