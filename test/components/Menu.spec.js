import React from 'react';
import Enzyme, { shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { BrowserRouter as Router } from 'react-router-dom';
import renderer from 'react-test-renderer';
import Menu from '../../app/components/Menu';

Enzyme.configure({ adapter: new Adapter() });

function setup() {
  const component = shallow(<Menu />);
  return {
    component,
    h2: component.find('h2'),
    links: component.find('Link'),
    exit: component.find('a')
  };
}

describe('Menu component', () => {
  it('should have correct name', () => {
    const { h2 } = setup();
    expect(h2.text()).toEqual('Menu');
  });

  it('should have three navigation links and exit link', () => {
    const { links, exit } = setup();
    expect(links.length).toEqual(3);
    expect(exit.length).toEqual(1);
    expect(exit.text()).toEqual('Exit');
  });

  it('should match exact snapshot', () => {
    const menu = (
      <Router>
        <Menu />
      </Router>
    );
    const tree = renderer.create(menu).toJSON();

    expect(tree).toMatchSnapshot();
  });
});
