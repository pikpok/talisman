// @flow
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import { firestoreReducer } from 'redux-firestore';
import counter from './counter';
import lobby from './lobby';

export default function createRootReducer(history: History) {
  return combineReducers({
    router: connectRouter(history),
    counter,
    lobby,
    firestore: firestoreReducer
  });
}
