// @flow
import {
  ADD_PLAYER,
  REMOVE_PLAYER,
  UPDATE_PLAYER,
  SET_ROUND_LIMIT,
  SET_ROUND_TIME,
  RESET_LOBBY_PLAYERS
} from '../actions/lobby';
import type { Action } from './types';
import { playerColors } from '../constants/playerColors';
import { ROUND_LIMIT, ROUND_TIMES } from '../constants/game';

const nanoid = require('nanoid');

export type LobbyAction = {
  ...Action,
  index?: number,
  player?: Player,
  roundLimit?: number,
  roundTime?: number
};

export type Player = {
  id: number,
  name: string,
  color: string,
  character: Character,
  position: {
    ring: 'outer' | 'inner',
    tile: number
  },
  equipment?: any[], // eslint-disable-line flowtype/no-weak-types
  exp: number
};

export type Character = {
  id: string,
  name: string,
  avatar: string,
  hp: number,
  gold: number,
  strength: number,
  magic: number
};

export type LobbyState = {
  players: Player[],
  roundLimit: number,
  roundTime: number
};

const defaultPlayerList: Player[] = [
  {
    id: nanoid(),
    name: 'Player 1',
    color: playerColors[0],
    character: {
      id: '6AGEQLDXYhwg0sAaKnrO',
      name: 'Barbarian',
      avatar: 'character_barbarian.png',
      hp: 8,
      gold: 0,
      strength: 4,
      magic: 1
    },
    position: { ring: 'outer', tile: 0 },
    exp: 0
  },
  {
    id: nanoid(),
    name: 'Player 2',
    color: playerColors[1],
    character: {
      id: '6AGEQLDXYhwg0sAaKnrO',
      name: 'Barbarian',
      avatar: 'character_barbarian.png',
      hp: 8,
      gold: 0,
      strength: 4,
      magic: 1
    },
    position: { ring: 'outer', tile: 0 },
    exp: 0
  }
];

const initialState: LobbyState = {
  players: defaultPlayerList.map(el => ({ ...el })),
  roundLimit: Number(ROUND_LIMIT[1].value),
  roundTime: Number(ROUND_TIMES[1].value)
};

const findFirstUnusedColor = (players: Player[]): string => {
  const usedColors = players.map(({ color }) => color);
  return playerColors.find(c => usedColors.indexOf(c) === -1);
};

export default function lobby(
  state: LobbyState = initialState,
  action: LobbyAction
) {
  switch (action.type) {
    case ADD_PLAYER:
      return {
        ...state,
        players: [
          ...state.players,
          {
            id: nanoid(),
            name: 'New player',
            color: findFirstUnusedColor(state.players),
            character: null,
            position: { ring: 'outer', tile: 0 }
          }
        ]
      };
    case REMOVE_PLAYER:
      return {
        ...state,
        players: [
          ...state.players.slice(0, action.index),
          ...state.players.slice(action.index + 1)
        ]
      };
    case UPDATE_PLAYER:
      return {
        ...state,
        players: [
          ...state.players.slice(0, action.index),
          action.player,
          ...state.players.slice(action.index + 1)
        ]
      };
    case SET_ROUND_LIMIT:
      return {
        ...state,
        roundLimit: action.roundLimit
      };
    case SET_ROUND_TIME:
      return {
        ...state,
        roundTime: action.roundTime
      };
    case RESET_LOBBY_PLAYERS:
      return {
        ...state,
        players: defaultPlayerList.map(el => Object.assign(el))
      };
    default:
      return state;
  }
}
