import React from 'react';
import styles from './CardSelector.css';
import { Card } from './Card';

type Props = {
  cards: CardData
};

export class CardSelector extends React.Component<Props> {
  state = {
    pivot: 0
  };

  componentDidUpdate(prevProps) {
    const { cards } = this.props;
    if (cards !== prevProps.cards) {
      this.resetPivot();
    }
  }

  movePivot = (side: string) => {
    switch (side) {
      case 'prev':
        this.setState(prev => ({ pivot: prev.pivot - 1 }));
        break;

      case 'next':
        this.setState(prev => ({ pivot: prev.pivot + 1 }));
        break;

      default:
        break;
    }
  };

  resetPivot() {
    this.setState({ pivot: 0 });
  }

  renderCard = (card: CardData, index: number, activeIndex: number) => (
    <Card key={card.id} data={card} darkMode={index !== activeIndex} />
  );

  renderCards = () => {
    const { cards } = this.props;
    const { pivot } = this.state;
    const lastIndex = cards.length - 1;
    let activeCardIndex = 1;
    let visibleCards: CardData[] = [];
    if (lastIndex >= 0) {
      visibleCards =
        cards.length <= 3
          ? cards
          : cards.filter((card, index) => {
              const diff = pivot - index;
              activeCardIndex = pivot === 0 ? 0 : 1;
              if (pivot === 0) {
                return diff >= -1 && diff <= 0;
              }
              if (pivot === lastIndex) {
                return diff <= 1 && diff >= 0;
              }
              return Math.abs(diff) < 2;
            });
    }

    return visibleCards.map((card, index) =>
      this.renderCard(card, index, activeCardIndex)
    );
  };

  render() {
    const { pivot } = this.state;
    const {
      cards: { length }
    } = this.props;
    return (
      <div
        style={{ display: 'flex', flexDirection: 'row', flexWrap: 'nowrap' }}
      >
        {pivot > 0 && (
          <button
            className={styles.scrollButton}
            type="button"
            onClick={() => this.movePivot('prev')}
          >
            {'<'}
          </button>
        )}
        {this.renderCards()}
        {pivot < length - 1 && (
          <button
            className={styles.scrollButton}
            type="button"
            onClick={() => this.movePivot('next')}
          >
            {'>'}
          </button>
        )}
      </div>
    );
  }
}
