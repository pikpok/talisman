// @flow
import { remote } from 'electron';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import routes from '../constants/routes';
import styles from './Menu.css';

type Props = {};

export default class Menu extends Component<Props> {
  props: Props;

  handleExit = () => {
    remote.getCurrentWindow().close();
  };

  render() {
    return (
      <div className={styles.menuContainer} data-tid="container">
        <Link to={routes.LOBBY}>Start new game</Link>
        <Link to={routes.RULES}>Rules</Link>
        <Link to={routes.SCORES}>Scores</Link>
        <a href="#" onClick={this.handleExit}>
          Exit
        </a>
      </div>
    );
  }
}
