// @flow
import React from 'react';
import type { Player } from '../reducers/lobby';
import styles from './EndGamePopup.css';

type Props = {
  handleClose(n: number): void,
  player: Player
};

class EndGamePopup extends React.Component<Props> {
  render() {
    const { player, handleClose } = this.props;
    return (
      <div className={styles.popupContainer}>
        <div className={styles.popupHeader}>End of the game</div>
        <div className={styles.popupContent}>
          <div className={styles.player}>
            <div
              className={styles.playerColor}
              style={{ backgroundColor: player.color }}
            />
            <div className={styles.playerName}>{player.name} won!</div>
          </div>
          <div className={styles.popupContent}>
            <button type="button" onClick={handleClose}>
              Close
            </button>
          </div>
        </div>
      </div>
    );
  }
}

export default EndGamePopup;
