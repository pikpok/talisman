import React from 'react';
import { Card } from './Card';
import styles from './ActionPopup.css';

type Props = {
  handleFight(): void,
  handleEquip(): void,
  // eslint-disable-next-line flowtype/no-weak-types
  action: any
};

export class ActionPopup extends React.Component<Props> {
  renderCombat = () => {
    const { handleFight } = this.props;
    return (
      <div>
        <div className={styles.textWrapper}>
          <h1 className={styles.textHeader}>Combat</h1>
          <p>
            On your way, you met a terrifying creature. Are you ready for the
            combat?
          </p>
        </div>
        <div className={styles.actions}>
          <button type="button" onClick={() => handleFight(true)}>
            {'Fight!'}
          </button>
        </div>
      </div>
    );
  };

  renderItem = () => {
    const {
      handleEquip,
      action: { card }
    } = this.props;
    return (
      <div>
        <div className={styles.textWrapper}>
          <h1 className={styles.textHeader}>Item</h1>
          <p>
            On your way, you&apos;ve found something in the bush. Do you want to
            equip this?
          </p>
        </div>
        <div className={styles.actions}>
          <button type="button" onClick={() => handleEquip(card)}>
            {'Equip'}
          </button>
        </div>
      </div>
    );
  };

  renderGold = () => {
    const {
      handleEquip,
      action: { card }
    } = this.props;
    return (
      <div>
        <div className={styles.textWrapper}>
          <h1 className={styles.textHeader}>Gold</h1>
          <p>Hurray! You&apos;ve earned some gold!</p>
        </div>
        <div className={styles.actions}>
          <button type="button" onClick={() => handleEquip(card)}>
            {'Take'}
          </button>
        </div>
      </div>
    );
  };

  renderContent = type => {
    switch (type) {
      case 'gold':
        return this.renderGold();
      case 'spirit':
      case 'monster':
        return this.renderCombat();
      case 'weapon':
      case 'spell':
        return this.renderItem();

      default:
        return null;
    }
  };

  render() {
    const { action } = this.props;
    return (
      <div className={styles.actionPopup}>
        <Card data={action.card} />
        <div className={styles.content}>
          {this.renderContent(action.card.type)}
        </div>
      </div>
    );
  }
}
