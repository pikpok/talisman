import React from 'react';
import styles from './Card.css';

type Props = {
  data: CardData,
  darkMode: boolean
};

export class Card extends React.Component<Props> {
  render() {
    const { data, darkMode } = this.props;
    return (
      <div className={styles.card}>
        <img src={`./assets/cards/${data.asset}`} alt={data.name} />
        <div className={styles.dataWrapper}>
          <div className={styles.cardName}>{data.name}</div>
          <div>{data.description}</div>
        </div>
        <div className={styles.cardPower}>{data.value}</div>
        {darkMode && <span className={styles.shadow} />}
      </div>
    );
  }
}
