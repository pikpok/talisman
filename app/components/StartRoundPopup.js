// @flow
import React from 'react';
import type { Player } from '../reducers/lobby';
import styles from './StartRoundPopup.css';

import dice1 from '../assets/hud/dice/die-1.jpg';
import dice2 from '../assets/hud/dice/die-2.jpg';
import dice3 from '../assets/hud/dice/die-3.jpg';
import dice4 from '../assets/hud/dice/die-4.jpg';
import dice5 from '../assets/hud/dice/die-5.jpg';
import dice6 from '../assets/hud/dice/die-6.jpg';

const dice = [dice1, dice2, dice3, dice4, dice5, dice6];

type Props = {
  handleClose(n: number): void,
  player: Player
};

type State = {
  currentRoll: number,
  diceState: $Values<typeof DICE_STATE>
};

const DICE_STATE = {
  BEFORE_ROLL: 0,
  ROLLING: 1,
  AFTER_ROLL: 2
};

class StartRoundPopup extends React.Component<Props, State> {
  state = {
    currentRoll: 0,
    diceState: DICE_STATE.BEFORE_ROLL
  };

  randomNumber = () => Math.round(Math.random() * 5);

  roll = () => {
    this.setState({ diceState: DICE_STATE.ROLLING });

    const rollAction = setInterval(() => {
      let currentRoll = this.randomNumber();
      this.setState(prev => {
        if (prev.currentRoll === currentRoll) {
          currentRoll = currentRoll < 5 ? currentRoll + 1 : currentRoll - 1;
        }
        return { currentRoll };
      });
    }, 200);

    setTimeout(() => {
      clearInterval(rollAction);
      this.setState({ diceState: DICE_STATE.AFTER_ROLL });
    }, 3000);
  };

  applyRoll = () => {
    const { currentRoll } = this.state;
    const { handleClose } = this.props;
    handleClose(currentRoll + 1);
  };

  render() {
    const { currentRoll, diceState } = this.state;
    const { player } = this.props;
    return (
      <div className={styles.popupContainer}>
        <div className={styles.popupHeader}>New turn</div>
        <div className={styles.popupContent}>
          <div className={styles.upperRow}>
            <div className={styles.player}>
              <div
                className={styles.playerColor}
                style={{ backgroundColor: player.color }}
              />
              <div className={styles.playerName}>{player.name}</div>
            </div>
            {diceState === DICE_STATE.BEFORE_ROLL && (
              <button onClick={this.roll} type="button">
                Roll!
              </button>
            )}
            {diceState === DICE_STATE.AFTER_ROLL && (
              <button type="button" onClick={this.applyRoll}>
                Close
              </button>
            )}
          </div>
          <div>
            {diceState !== DICE_STATE.BEFORE_ROLL && (
              <img
                className={styles.dieStyle}
                src={dice[currentRoll]}
                alt={`dice-${currentRoll}`}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default StartRoundPopup;
