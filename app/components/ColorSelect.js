import React from 'react';
import { connect } from 'react-redux';
import type { Player } from '../reducers/lobby';
import { playerColors } from '../constants/playerColors';
import styles from './Select.css';

type Props = {
  value: string,
  onChange(value: string): void,
  players: Player[]
};

const checkIfUsed = (value: string, players: Player[]) =>
  players && players.map(({ color }) => color).indexOf(value) > -1;

const _ColorSelect = ({ value, players, onChange }: Props) => (
  <select className={styles.select} onChange={onChange} value={value}>
    {playerColors.map(color => (
      <option
        value={color}
        disabled={value !== color && checkIfUsed(color, players)}
        key={color}
      >
        {color.toUpperCase()}
      </option>
    ))}
  </select>
);

const mapState = ({ lobby }) => ({
  players: lobby.players
});

export const ColorSelect = connect(mapState)(_ColorSelect);
