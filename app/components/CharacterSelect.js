import React from 'react';
import type { Player, Character } from '../reducers/lobby';
import styles from './Select.css';

type Props = {
  value: string,
  onChange(value: string): void,
  players: Player[],
  characters: Character[]
};

const checkIfUsed = (value: string, players: Player[]) =>
  players && players.map(({ character }) => character).indexOf(value) > -1;

const _CharacterSelect = ({ value, players, characters, onChange }: Props) => {
  if (!value && characters.length > 0) {
    onChange({ target: { value: characters[0].id } });
  }

  return (
    <select className={styles.select} onChange={onChange} value={value}>
      {characters.map(character => (
        <option
          value={character.id}
          disabled={value !== character && checkIfUsed(character.id, players)}
          key={character.id}
        >
          {character.name.toUpperCase()}
        </option>
      ))}
    </select>
  );
};

export const CharacterSelect = _CharacterSelect;
