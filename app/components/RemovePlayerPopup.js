import React from 'react';
import styles from './ActionPopup.css';
/* eslint-disable */

class RemovePlayerPopup extends React.Component {
  render() {
    const { player, handleClose } = this.props;
    return (
      <div className={styles.actionPopup + ' ' + styles.newLevel}>
        <div className={styles.content}>
          <div>
            <div className={styles.textWrapper}>
              <h1 className={styles.textHeader}>You've lost</h1>
              <img src={`./assets/${player.character.avatar}`} width={100} height={150}/>
              <p>
                {player.name} you've lost. You will be removed from game :(
              </p>
              <div className={styles.actions}>
                <button type="button" onClick={handleClose}>
                  {'Close'}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default RemovePlayerPopup;
