import React from 'react';
import styles from './ActionPopup.css';
import { Card } from './Card';

type Props = {
  handleClose(): void,
  handleMovePlayerToInner(): void,
  handlePlayerHeal(): void,
  handleMovePlayerToBoss(): void,
  handleExit(): void,
  handleBossFight(): void,
  // eslint-disable-next-line flowtype/no-weak-types
  specialType: any
};

export class SpecialPopup extends React.Component<Props> {
  renderCity = () => {
    const { handleMovePlayerToInner, handlePlayerHeal } = this.props;
    return (
      <div>
        <div className={styles.textWrapper}>
          <h1 className={styles.textHeader}>City</h1>
          <p>
            You arrive safely inside the city walls. Innkeeper smiles and offers
            you a rest, but wait, there is a shady man selling a map to the
            temple.
          </p>
        </div>
        <div className={styles.actions}>
          <button type="button" onClick={() => handlePlayerHeal()}>
            {'Rest for 1 GP'}
          </button>
          <button type="button" onClick={() => handleMovePlayerToInner()}>
            {'Buy map for 5GP'}
          </button>
        </div>
      </div>
    );
  };

  renderEntrance = () => {
    const { handleClose, handleMovePlayerToBoss } = this.props;
    return (
      <div>
        <div className={styles.textWrapper}>
          <h1 className={styles.textHeader}>Temple Entrance</h1>
          <p> Before you lies the fabled Temple. Dare You enter it? </p>
        </div>
        <div className={styles.actions}>
          <button type="button" onClick={() => handleMovePlayerToBoss()}>
            {'Enter the temple'}
          </button>
          <button type="button" onClick={() => handleClose(true)}>
            {'Turn back'}
          </button>
        </div>
      </div>
    );
  };

  renderBoss = () => {
    const { handleBossFight } = this.props;
    return (
      <div>
        <div className={styles.textWrapper}>
          <h1 className={styles.textHeader}>Final trial</h1>
          <p>
            The Temple is right there, but wait! Black dragon! There is no
            turning back now.
          </p>
        </div>
        <div className={styles.actions}>
          <button type="button" onClick={handleBossFight}>
            {'Fight'}
          </button>
        </div>
      </div>
    );
  };

  renderBossCard = () => {
    const card = {
      asset: 'boss-dragon.jpg',
      name: 'Black dragon',
      description: 'Ancient dragon, awaken from slumber.',
      value: 7
    };

    return <Card data={card} />;
  };

  renderEntranceCard = () => {
    const card = {
      asset: 'boss-trial.jpg',
      name: 'Temple entrance',
      description: 'Steps to the fabled Temple. What lies ahead is a mystery',
      value: '-'
    };

    return <Card data={card} />;
  };

  renderCityCard = () => {
    const card = {
      asset: 'city-inn.jpg',
      name: 'City inn',
      description: 'A place of rest for a weary traveller.',
      value: '-'
    };

    return <Card data={card} />;
  };

  renderWinCard = () => {
    const card = {
      asset: 'boss-win.jpg',
      name: 'The crown',
      description: 'Your quest is over, the crown in your hands.',
      value: '-'
    };

    return <Card data={card} />;
  };

  renderWin = () => {
    const { handleExit } = this.props;
    return (
      <div>
        <div className={styles.textWrapper}>
          <h1 className={styles.textHeader}>The crown</h1>
          <p>
            You have finally reached the top of the Temple. The Crown is Yours.
          </p>
        </div>
        <div className={styles.actions}>
          <button type="button" onClick={() => handleExit(true)}>
            {'You win!'}
          </button>
        </div>
      </div>
    );
  };

  renderContent = () => {
    const { specialType } = this.props;
    switch (specialType) {
      case 'city':
        return this.renderCity();
      case 'innerEntrance':
        return this.renderEntrance();
      case 'trial':
        return this.renderBoss();
      case 'end':
        return this.renderWin();
      default:
    }
  };

  renderCard = () => {
    const { specialType } = this.props;
    switch (specialType) {
      case 'city':
        return this.renderCityCard();
      case 'innerEntrance':
        return this.renderEntranceCard();
      case 'trial':
        return this.renderBossCard();
      case 'end':
        return this.renderWinCard();
      default:
    }
  };

  render() {
    return (
      <div className={styles.actionPopup}>
        {this.renderCard()}
        <div className={styles.content}>{this.renderContent()}</div>
      </div>
    );
  }
}
