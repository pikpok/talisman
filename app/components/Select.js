// @flow
import React from 'react';
import type { Option } from '../constants/types';
import styles from './Select.css';

type Props = {
  ...HTMLSelectElement,
  options: Option[],
  onChange: (value: string) => void
};

export const Select = ({ onChange, options, ...props }: Props) => (
  <select
    className={styles.select}
    onChange={({ target: { value } }) => onChange(value)}
    {...props}
  >
    {options.map(option => (
      <option key={option.value} value={option.value}>
        {option.label}
      </option>
    ))}
  </select>
);
