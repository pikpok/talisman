import React from 'react';
import styles from './EquipmentPopup.css';
import { CardSelector } from './CardSelector';

type Props = {
  playerItems: Card[],
  handleClose: any
};

export class EquipmentPopup extends React.Component<Props> {
  render() {
    const { playerItems, handleClose } = this.props;

    return (
      <div className={styles.popupContainer}>
        <div className={styles.popupHeader}>Items</div>
        <div className={styles.popupContent}>
          <CardSelector cards={playerItems} />
        </div>
        <button
          className={styles.closePopup}
          type="button"
          onClick={handleClose}
        >
          Close
        </button>
      </div>
    );
  }
}
