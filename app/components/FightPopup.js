import React from 'react';
import { Card } from './Card';
import styles from './FightPopup.css';

import dice1 from '../assets/hud/dice/die-1.jpg';
import dice2 from '../assets/hud/dice/die-2.jpg';
import dice3 from '../assets/hud/dice/die-3.jpg';
import dice4 from '../assets/hud/dice/die-4.jpg';
import dice5 from '../assets/hud/dice/die-5.jpg';
import dice6 from '../assets/hud/dice/die-6.jpg';

const dice = [dice1, dice2, dice3, dice4, dice5, dice6];

type Props = {
  player: any,
  enemy: any,
  handleClose: void
};

type State = {
  phase: string,
  enemyRoll: number,
  playerRoll: number,
  diceState: $Values<typeof DICE_STATE>
};

const DICE_STATE = {
  BEFORE_ROLL: 0,
  ROLLING: 1,
  AFTER_ROLL: 2
};

class FightPopup extends React.Component<Props, State> {
  state = {
    phase: 'start',
    enemyRoll: 0,
    playerRoll: 0,
    diceState: DICE_STATE.BEFORE_ROLL
  };

  componentDidMount() {
    this.setState({ phase: 'start', diceState: DICE_STATE.BEFORE_ROLL });
  }

  randomNumber = () => Math.round(Math.random() * 5);

  roll = isMagicAttack => {
    const { player, enemy } = this.props;

    this.setState({ diceState: DICE_STATE.ROLLING });

    const rollAction = setInterval(() => {
      let enemyRoll = this.randomNumber();
      let playerRoll = this.randomNumber();
      this.setState(prev => {
        if (prev.enemyRoll === enemyRoll)
          enemyRoll = enemyRoll < 5 ? enemyRoll + 1 : enemyRoll - 1;
        if (prev.playerRoll === playerRoll)
          playerRoll = playerRoll < 5 ? playerRoll + 1 : playerRoll - 1;
        return { enemyRoll, playerRoll };
      });
    }, 300);

    setTimeout(() => {
      clearInterval(rollAction);
      this.setState(prev => {
        const enemyScore = prev.enemyRoll + enemy.value;
        const playerScore =
          prev.playerRoll +
          (isMagicAttack ? player.character.magic : player.character.strength);
        return {
          diceState: DICE_STATE.AFTER_ROLL,
          phase: enemyScore < playerScore ? 'win' : 'loose',
          enemyScore,
          playerScore
        };
      });
    }, 3000);
  };

  renderContent() {
    const { enemy, player } = this.props;
    const { enemyRoll, playerRoll, diceState, phase } = this.state;
    const isBoss = enemy.type === 'boss';
    return (
      <div className={styles.fightView}>
        <div className={styles.enemies}>
          <div>
            <Card data={enemy} />
            {diceState !== DICE_STATE.BEFORE_ROLL && (
              <img
                className={styles.diceStyle}
                src={dice[enemyRoll]}
                alt={`dice-${enemyRoll}`}
              />
            )}
          </div>
          <div className={styles.playerBox}>
            <img
              src={`./assets/${player.character.avatar}`}
              alt="character"
              className={styles.playerAvatar}
            />
            <div className={styles.player}>
              <div
                className={styles.playerColor}
                style={{ backgroundColor: player.color }}
              />
              <div className={styles.playerName}>{player.name}</div>
            </div>
            {diceState !== DICE_STATE.BEFORE_ROLL && (
              <img
                className={styles.diceStyle}
                src={dice[playerRoll]}
                alt={`dice-${playerRoll}`}
              />
            )}
          </div>
        </div>
        {diceState === DICE_STATE.BEFORE_ROLL && isBoss && (
          <div className={styles.actions}>
            <button onClick={() => this.roll()} type="button">
              Strength attack
            </button>
            <button onClick={() => this.roll(true)} type="button">
              Magic attack
            </button>
          </div>
        )}
        {diceState === DICE_STATE.BEFORE_ROLL && !isBoss && (
          <div className={styles.actions}>
            <button
              onClick={() => this.roll(enemy.type === 'spirit')}
              type="button"
            >
              Attack
            </button>
          </div>
        )}

        {phase === 'win' && this.renderWinContent()}
        {phase === 'loose' && this.renderLooseContent()}
      </div>
    );
  }

  renderWinContent = () => {
    const { handleClose, enemy } = this.props;
    return (
      <div className={styles.score}>
        <p className={styles.scoreText}>You have slain enemy!</p>
        <div className={styles.actions}>
          <button
            type="button"
            onClick={() => handleClose(true, enemy.type === 'boss')}
          >
            Close
          </button>
        </div>
      </div>
    );
  };

  renderLooseContent = () => {
    const { handleClose, enemy } = this.props;
    return (
      <div className={styles.score}>
        <p className={styles.scoreText}>Enemy won!</p>
        <div className={styles.actions}>
          <button
            type="button"
            onClick={() => handleClose(false, enemy.type === 'boss')}
          >
            Close
          </button>
        </div>
      </div>
    );
  };

  render() {
    return (
      <div className={styles.fightPopup}>
        <div className={styles.container}>
          <div className={styles.content}>
            <div className={styles.popupHeader}>Battle</div>
            {this.renderContent()}
          </div>
        </div>
      </div>
    );
  }
}

export default FightPopup;
