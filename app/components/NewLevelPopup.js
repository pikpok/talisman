import React from 'react';
import { STAT_TYPE } from '../constants/game';
import styles from './ActionPopup.css';
/* eslint-disable */

class NewLevelPopup extends React.Component {
  magicRaise = () => this.props.raise(STAT_TYPE.magic);

  goldRaise = () => this.props.raise(STAT_TYPE.gold);

  strengthRaise = () => this.props.raise(STAT_TYPE.strength);

  render() {
    return (
      <div className={styles.actionPopup + " " + styles.newLevel}>
        <div className={styles.content}>
          <div>
            <div className={styles.textWrapper}>
              <h1 className={styles.textHeader}>New level</h1>
              <p>
                Hurray! You've gain new level!
                Which statistic do you wanna raise?
              </p>
              <div className={styles.actions}>
                <button type="button" onClick={this.strengthRaise}>
                  {'Strength'}
                </button>
                <button type="button" onClick={this.magicRaise}>
                  {'Magic'}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default NewLevelPopup;
