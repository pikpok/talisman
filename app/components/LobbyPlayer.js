// @flow
import React from 'react';
import type { Player, Character } from '../reducers/lobby';
import { ColorSelect } from './ColorSelect';
import { CharacterSelect } from './CharacterSelect';
import styles from './LobbyPlayer.css';

type Props = {
  player: Player,
  deletePlayer: () => void,
  deleteAllowed: boolean,
  onChange: (player: Player) => void,
  characters: Character[]
};

class _LobbyPlayer extends React.Component<Props> {
  updateColor(color: string) {
    const { player, onChange } = this.props;

    onChange({ ...player, color });
  }

  updateName(name: string) {
    const { player, onChange } = this.props;

    onChange({ ...player, name });
  }

  updateCharacter(characterId: string) {
    const { player, onChange, characters } = this.props;
    const character = characters.find(char => char.id === characterId);
    if (character) {
      onChange({
        ...player,
        name: character.name,
        character: { ...character }
      });
    }
  }

  render() {
    const { player, deletePlayer, deleteAllowed, characters } = this.props;
    return (
      <div className={styles.wrapper}>
        <div
          className={styles.playerIco}
          style={{ backgroundColor: player.color }}
        />
        <input
          className={styles.inputName}
          value={player.name}
          onChange={e => this.updateName(e.target.value)}
        />
        <CharacterSelect
          value={player.character ? player.character.id : ''}
          characters={characters}
          onChange={e => this.updateCharacter(e.target.value)}
        />
        <ColorSelect
          value={player.color}
          onChange={e => this.updateColor(e.target.value)}
        />
        {deleteAllowed && (
          <button
            className={styles.deleteButton}
            type="button"
            onClick={deletePlayer}
          >
            Delete
          </button>
        )}
      </div>
    );
  }
}

export const LobbyPlayer = _LobbyPlayer;
