// @flow
import type { LobbyAction, Player } from '../reducers/lobby';

export const ADD_PLAYER = 'ADD_PLAYER';
export const REMOVE_PLAYER = 'REMOVE_PLAYER';
export const UPDATE_PLAYER = 'UPDATE_PLAYER';
export const SET_ROUND_LIMIT = 'SET_ROUND_LIMIT';
export const SET_ROUND_TIME = 'SET_ROUND_TIME';
export const RESET_LOBBY_PLAYERS = 'RESET_LOBBY_PLAYERS';

export function addPlayer(): LobbyAction {
  return {
    type: ADD_PLAYER
  };
}

export function removePlayer(index: number): LobbyAction {
  return {
    type: REMOVE_PLAYER,
    index
  };
}

export function updatePlayer(player: Player, index: number): LobbyAction {
  return {
    type: UPDATE_PLAYER,
    player,
    index
  };
}

export function setRoundLimit(roundLimit: number): LobbyAction {
  return {
    type: SET_ROUND_LIMIT,
    roundLimit
  };
}

export function setRoundTime(roundTime: number): LobbyAction {
  return {
    type: SET_ROUND_TIME,
    roundTime
  };
}

export function resetLobbyPlayers(): LobbyAction {
  return {
    type: RESET_LOBBY_PLAYERS
  };
}
