import firebase from 'firebase/app';
import { reduxFirestore } from 'redux-firestore';
import 'firebase/auth';
import 'firebase/database';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyCBZ0ONBmEIbJNxIQVW9GfDrXEkY76C49U',
  authDomain: 'talisman-1643c.firebaseapp.com',
  databaseURL: 'https://talisman-1643c.firebaseio.com',
  projectId: 'talisman-1643c',
  storageBucket: 'talisman-1643c.appspot.com',
  messagingSenderId: '578465858026'
}; // from Firebase Console

// Initialize firebase instance
firebase.initializeApp(firebaseConfig);
// Initialize Cloud Firestore through Firebase
firebase.firestore();

// Add reduxFirestore store enhancer to store creator
export const createStoreWithFirebase = () => reduxFirestore(firebase);
