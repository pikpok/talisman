import React from 'react';
import { Route, Switch, Redirect } from 'react-router';
import routes from './constants/routes';
import App from './containers/App';
import HomePage from './containers/HomePage';
import Rules from './containers/Rules';
import Scores from './containers/Scores';
import Lobby from './containers/Lobby';
import { Canvas } from './containers/Canvas';

const MenuRoutes = () => (
  <App>
    <Switch>
      <Route path={routes.HOME} component={HomePage} />
      <Route path={routes.RULES} component={Rules} />
      <Route path={routes.SCORES} component={Scores} />
      <Route path={routes.LOBBY} component={Lobby} />
      <Redirect to={routes.HOME} />
    </Switch>
  </App>
);

export default () => (
  <Switch>
    <Route path={routes.GAME} component={Canvas} />
    <Route path="/" component={MenuRoutes} />
  </Switch>
);
