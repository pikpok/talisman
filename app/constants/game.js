import type { Option } from './types';

export const ROUND_TIMES: Option[] = [
  { label: '5 sec', value: '5' },
  { label: '1 min', value: '60' },
  { label: '2 min', value: '120' },
  { label: '5 min', value: '300' }
];

export const ROUND_LIMIT: Option[] = [
  { label: '1', value: '1' },
  { label: '10', value: '10' },
  { label: '20', value: '20' },
  { label: '50', value: '50' }
];

export const STAT_TYPE = {
  magic: 0,
  strength: 1
};

export const EXPERIENCE_CAP = 7;
