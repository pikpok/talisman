// @flow
export type Option = {
  label: string,
  value: string
};

export type ItemType =
  | 'ENEMY'
  | 'MAGIC_ENEMY'
  | 'GOLD'
  | 'WEAPON'
  | 'MAGIC_ITEM';

export type Card = {
  type: ItemType,
  name: string,
  image: string,
  description: string,
  value: number
};
