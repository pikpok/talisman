export const itemTypes = {
  ENEMY: 'ENEMY',
  MAGIC_ENEMY: 'MAGIC_ENEMY',
  GOLD: 'GOLD',
  MAGIC_ITEM: 'MAGIC_ITEM',
  WEAPON: 'WEAPON'
};
