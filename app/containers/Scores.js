import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import type { Character } from '../reducers/lobby';
import styles from './Scores.css';
import { CharacterSelect } from '../components/CharacterSelect';

export type Score = {
  character: string,
  color: string,
  name: string,
  score: number
};

type Props = {
  scores: Score[],
  characters: Character[]
};

type State = {
  filters: {
    character?: string
  }
};

class Scores extends Component<Props, State> {
  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  state = {
    filters: {
      character: null
    }
  };

  componentDidMount() {
    const {
      store: { firestore }
    } = this.context;
    this.fetchData();
    firestore.get({ collection: 'characters' });
  }

  componentDidUpdate(prevProps, prevState) {
    const { filters } = this.state;
    if (JSON.stringify(prevState.filters) !== JSON.stringify(filters)) {
      this.fetchData();
    }
  }

  fetchData = () => {
    const {
      store: { firestore }
    } = this.context;
    const { filters } = this.state;
    const conditions = [];
    if (filters.character)
      conditions.push(['character', '==', filters.character]);
    firestore.get({
      collection: 'ranking',
      orderBy: ['score', 'desc'],
      where: conditions
    });
  };

  handleCharacterChange = characterId => {
    const { characters } = this.props;
    const { filters } = this.state;
    const character = characters.find(char => char.id === characterId);
    if (characterId === -1 && !filters.character) return;
    if (
      characters.length &&
      (!character || character.name !== filters.character)
    ) {
      this.setState(state => ({
        filters: {
          ...state.filters,
          character: character ? character.name : null
        }
      }));
    }
  };

  handleNameChange = (name: string) => {
    this.setState(state => ({
      filters: {
        ...state.filters,
        name
      }
    }));
  };

  render() {
    const { scores, characters } = this.props;
    const { filters } = this.state;

    return (
      <React.Fragment>
        <div className={styles.header}> Scores </div>

        <div>
          <CharacterSelect
            characters={[
              { id: -1, name: 'Select character...' },
              ...characters
            ]}
            value={
              characters.find(char => char.name === filters.character)
                ? characters.find(char => char.name === filters.character).id
                : -1
            }
            onChange={e => this.handleCharacterChange(e.target.value)}
            players={[]}
          />
        </div>

        <div className={styles.wrapper}>
          <table className={styles.table} style={{ width: '100%' }}>
            <thead>
              <tr>
                <th />
                <th>Name</th>
                <th>Character</th>
                <th>Score</th>
              </tr>
            </thead>
            <tbody>
              {[...scores]
                .sort((a, b) => b.score - a.score)
                .map((score, index) => (
                  <tr key={score.id}>
                    <td className={styles.gridHeader}>{index + 1}</td>
                    <td className={styles.grid}>{score.name}</td>
                    <td className={styles.grid}>{score.character}</td>
                    <td className={styles.grid}>{score.score}</td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </React.Fragment>
    );
  }
}

export default connect(state => ({
  scores: state.firestore.ordered.ranking || [],
  characters: state.firestore.ordered.characters || []
}))(Scores);
