// @flow
import * as React from 'react';
import { withRouter } from 'react-router';
import type { ContextRouter } from 'react-router';
import { Link } from 'react-router-dom';
import styles from './App.css';

type Props = {
  ...ContextRouter,
  children: React.Node
};

class App extends React.Component<Props> {
  props: Props;

  render() {
    const {
      children,
      location: { pathname }
    } = this.props;
    return (
      <>
        <div className={styles.logo} />
        <div className={styles.container}>
          {pathname !== '/home' && (
            <Link className={styles.backLink} data-tid="backButton" to="/">
              Back
            </Link>
          )}

          {children}
        </div>
      </>
    );
  }
}

export default withRouter(App);
