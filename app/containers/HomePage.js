// @flow
import React, { Component } from 'react';
import Menu from '../components/Menu';

type Props = {};

export default class HomePage extends Component<Props> {
  props: Props;

  render() {
    return <Menu />;
  }
}
