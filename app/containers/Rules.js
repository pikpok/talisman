import React, { Component } from 'react';
import ReactMarkdown from 'react-markdown';
import rules from '../assets/text/game_rules.txt';
import styles from './Rules.css';

export default class Rules extends Component {
  render() {
    return (
      <>
        <div className={styles.header}>Rules</div>
        <div className={styles.rulesWrapper}>
          <ReactMarkdown source={rules} />
        </div>
      </>
    );
  }
}
