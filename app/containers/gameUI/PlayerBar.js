import React, { Component } from 'react';
import styles from './PlayerBar.css';

type Props = {
  players: Player[]
};

class PlayerBar extends Component<Props> {
  // eslint-disable-next-line class-methods-use-this
  printPlayer(player) {
    const color = { backgroundColor: player.color };
    return (
      <div key={player.id} className={styles.player}>
        <div className={styles.playerInner}>
          <div className={styles.playerImg} style={color} />
          <div className={styles.playerData}>
            <div className={styles.playerName}>{player.name}</div>
            <div className={styles.playerStats}>
              <div className={styles.statCell}>
                <div className={[styles.label, styles.str].join(' ')}>S</div>
                <div>{player.character.strength}</div>
              </div>
              <div className={styles.statCell}>
                <div className={[styles.label, styles.mg].join(' ')}>M</div>
                <div>{player.character.magic}</div>
              </div>
              <div className={styles.statCell}>
                <div className={[styles.label, styles.hp].join(' ')}>HP</div>
                <div>{player.character.hp}</div>
              </div>
              <div className={styles.statCell}>
                <div className={[styles.label, styles.gp].join(' ')}>G</div>
                <div>{player.character.gold}</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { players } = this.props;
    return (
      <div className={styles.playerBar}>
        {players.map(player => this.printPlayer(player))}
      </div>
    );
  }
}

export default PlayerBar;
