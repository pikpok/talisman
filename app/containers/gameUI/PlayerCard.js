import React, { Component } from 'react';
import styles from './PlayerCard.css';
import { EXPERIENCE_CAP } from '../../constants/game';

class PlayerCard extends Component<Props> {
  render() {
    const { range, player } = this.props;
    return (
      <div className={styles.wrapper}>
        <div className={styles.player}>
          <div className={styles.playerName}>{player.name}</div>
          <div className={styles.className}>{player.character.name}</div>
        </div>
        <img
          className={styles.cardWrapper}
          src={`./assets/${player.character.avatar}`}
          alt="Character portrait"
        />
        <div className={styles.playerStats}>
          <div className={styles.movCell}>{range || '-'}</div>
          <div className={styles.statCell}>
            <div className={[styles.label, styles.str].join(' ')}>S</div>
            <div className={styles.value}>{player.character.strength}</div>
          </div>
          <div className={styles.statCell}>
            <div className={[styles.label, styles.mg].join(' ')}>M</div>
            <div className={styles.value}>{player.character.magic}</div>
          </div>
          <div className={styles.statCell}>
            <div className={[styles.label, styles.hp].join(' ')}>HP</div>
            <div className={styles.value}>{player.character.hp}</div>
          </div>
          <div className={styles.statCell}>
            <div className={[styles.label, styles.gp].join(' ')}>G</div>
            <div className={styles.value}>{player.character.gold}</div>
          </div>
          <div className={styles.lvlCell}>
            <div className={styles.lvlWrapper}>
              <div>{player.exp || 0}</div>
              <div>/{EXPERIENCE_CAP}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default PlayerCard;
