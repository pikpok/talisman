import React, { Component } from 'react';
import styles from './TimerBar.css';

type Props = {
  round: number,
  roundLimit: number,
  roundTime: number
};

class TimerBar extends Component<Props> {
  addLeadingZero = (n: number) => (n < 10 ? `0${n}` : `${n}`);

  formatTime = (seconds: number) =>
    `${this.addLeadingZero(Math.floor(seconds / 60))}:${this.addLeadingZero(
      seconds % 60
    )}`;

  render() {
    const { round, roundLimit, roundTime } = this.props;
    return (
      <div className={styles.wrapper}>
        <div className={styles.fieldRound}>
          <div className={styles.label}> ROUND </div>
          <div className={styles.value}>
            {round}/{roundLimit}
          </div>
        </div>
        <div className={styles.fieldTime}>
          <div className={styles.label}> TIME </div>
          <div className={styles.value}> {this.formatTime(roundTime)} </div>
        </div>
      </div>
    );
  }
}

export default TimerBar;
