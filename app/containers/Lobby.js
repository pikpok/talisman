// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import type { Dispatch } from 'redux';
import { Link } from 'react-router-dom';
import {
  addPlayer,
  removePlayer,
  updatePlayer,
  setRoundLimit,
  setRoundTime
} from '../actions/lobby';
import type {
  LobbyState,
  LobbyAction,
  Player,
  Character
} from '../reducers/lobby';
import routes from '../constants/routes.json';
import { LobbyPlayer } from '../components/LobbyPlayer';
import { Select } from '../components/Select';
import { ROUND_TIMES, ROUND_LIMIT } from '../constants/game';
import styles from './Lobby.css';

type Props = {
  dispatch: Dispatch<LobbyAction>,
  lobby: LobbyState,
  characters: Character[]
};

type State = {
  roundLimit: string,
  roundTime: string
};

class Lobby extends Component<Props, State> {
  static contextTypes = { store: PropTypes.object.isRequired };

  componentDidMount() {
    const {
      store: { firestore }
    } = this.context;
    firestore.get({ collection: 'characters', orderBy: 'name' });
  }

  componentDidUpdate(prevProps) {
    const {
      characters,
      dispatch,
      lobby: { players }
    } = this.props;

    if (prevProps.characters !== characters && characters.length > 0) {
      players.forEach((player, index) => {
        dispatch(
          updatePlayer(
            {
              ...player,
              character: characters[index],
              exp: 0
            },
            index
          )
        );
      });
    }
  }

  render() {
    const {
      dispatch,
      lobby: { players, roundLimit, roundTime },
      characters
    } = this.props;

    const addingAllowed = players.length < 6;
    return (
      <React.Fragment>
        <div className={styles.topWrapper}>
          <div className={styles.header}>Lobby</div>
          <div className={styles.gameParams}>
            <div className={styles.selector}>
              <label className={styles.selectorLable}>Round time</label>
              <Select
                options={ROUND_TIMES}
                value={`${roundTime}`}
                onChange={value => dispatch(setRoundTime(Number(value)))}
              />
            </div>

            <div className={styles.selector}>
              <label className={styles.selectorLable}>Round limit</label>
              <Select
                id="round-limit"
                options={ROUND_LIMIT}
                value={`${roundLimit}`}
                onChange={value => dispatch(setRoundLimit(Number(value)))}
              />
            </div>
          </div>

          <div className={styles.playerWrapper}>
            {players.map((player, index) => (
              <LobbyPlayer
                key={player.id}
                player={player}
                deletePlayer={() => dispatch(removePlayer(index))}
                deleteAllowed={players.length > 2}
                characters={characters}
                onChange={(updatedPlayer: Player) =>
                  dispatch(updatePlayer(updatedPlayer, index))
                }
              />
            ))}
          </div>

          <div className={styles.buttonRow}>
            {addingAllowed && (
              <button
                type="button"
                className={styles.button}
                onClick={() => dispatch(addPlayer())}
              >
                Add player
              </button>
            )}

            <div className={styles.button}>
              <Link className={styles.link} to={routes.GAME}>
                Start game
              </Link>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapState = ({ lobby, firestore }) => ({
  lobby,
  characters: firestore.ordered.characters || []
});

export default connect(mapState)(Lobby);
