// @flow
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import styles from './Canvas.css';
// eslint-disable-next-line import/extensions
import TimerBar from './gameUI/TimerBar.js';
// eslint-disable-next-line import/extensions
import PlayerBar from './gameUI/PlayerBar.js';
// eslint-disable-next-line import/extensions
import PlayerCard from './gameUI/PlayerCard.js';
import NewLevelPopup from '../components/NewLevelPopup';
import type { LobbyState, Player } from '../reducers/lobby';
// assets import
import bgcanvas from '../assets/bg_canvas.png';

import tilebgpaper from '../assets/tile_bg_paper.png';
import tilebgbrick from '../assets/tile_bg_brick.png';
import tilebgblack from '../assets/tile_bg_black.png';
import tilebggold from '../assets/tile_bg_gold.png';

import tilefinalend from '../assets/tile_final_trial.png';
import tilefinaltrial from '../assets/tile_final_end.png';

import tileinnerswampentrance from '../assets/tile_inner_swamp_entrance.png';
import tileinnerswampvertical1 from '../assets/tile_inner_swamp1_1.png';
import tileinnerswampvertical2 from '../assets/tile_inner_swamp1_2.png';
import tileinnerswampvertical3 from '../assets/tile_inner_swamp1_3.png';
import tileinnerswamphorizontal1 from '../assets/tile_inner_swamp2_1.png';
import tileinnerswamphorizontal2 from '../assets/tile_inner_swamp2_2.png';
import tileinnerswamphorizontal3 from '../assets/tile_inner_swamp2_3.png';
import tileinnerswamphorizontal4 from '../assets/tile_inner_swamp2_4.png';

import tileoutercity1 from '../assets/tile_outer_city1.png';
import tileoutercity2 from '../assets/tile_outer_city2.png';
import tileoutercity3 from '../assets/tile_outer_city3.png';
import tileoutercity4 from '../assets/tile_outer_city4.png';
import tileouterfield1 from '../assets/tile_outer_field1.png';
import tileouterfield2 from '../assets/tile_outer_field2.png';
import tileouterfield3 from '../assets/tile_outer_field3.png';
import tileouterfield4 from '../assets/tile_outer_field4.png';
import tileouterforest1 from '../assets/tile_outer_forest1.png';
import tileouterforest2 from '../assets/tile_outer_forest2.png';
import tileouterforest3 from '../assets/tile_outer_forest3.png';
import tileoutermountain1 from '../assets/tile_outer_moutain1.png';
import tileoutermountain2 from '../assets/tile_outer_moutain2.png';
import tileoutermountain3 from '../assets/tile_outer_moutain3.png';

import P1 from '../assets/ico/ico-P1.png';
import P2 from '../assets/ico/ico-P2.png';
import P3 from '../assets/ico/ico-P3.png';
import P4 from '../assets/ico/ico-P4.png';
import P5 from '../assets/ico/ico-P5.png';
import P6 from '../assets/ico/ico-P6.png';

import { resetLobbyPlayers } from '../actions/lobby';
import { EquipmentPopup } from '../components/EquipmentPopup';
import { ActionPopup } from '../components/ActionPopup';
import { SpecialPopup } from '../components/SpecialPopup';
import StartRoundPopup from '../components/StartRoundPopup';
import { STAT_TYPE, EXPERIENCE_CAP } from '../constants/game';
import FightPopup from '../components/FightPopup';
import RemovePlayerPopup from '../components/RemovePlayerPopup';
import EndGamePopup from '../components/EndGamePopup';

// eslint-disable-next-line flowtype/no-weak-types
declare var createjs: any;

const bgCanvas = new createjs.Bitmap(bgcanvas);

const tileOut = new createjs.Bitmap(tilebgpaper);
const tileCit = new createjs.Bitmap(tilebgbrick);
const tileIn = new createjs.Bitmap(tilebgblack);
const tileCr = new createjs.Bitmap(tilebggold);

const tileFinEnd = new createjs.Bitmap(tilefinalend);
const tileFinTrial = new createjs.Bitmap(tilefinaltrial);

const tileInSE = new createjs.Bitmap(tileinnerswampentrance);
const tileInSV1 = new createjs.Bitmap(tileinnerswampvertical1);
const tileInSV2 = new createjs.Bitmap(tileinnerswampvertical2);
const tileInSV3 = new createjs.Bitmap(tileinnerswampvertical3);
const tileInSH1 = new createjs.Bitmap(tileinnerswamphorizontal1);
const tileInSH2 = new createjs.Bitmap(tileinnerswamphorizontal2);
const tileInSH3 = new createjs.Bitmap(tileinnerswamphorizontal3);
const tileInSH4 = new createjs.Bitmap(tileinnerswamphorizontal4);

const playerIcons = [
  new createjs.Bitmap(P1),
  new createjs.Bitmap(P2),
  new createjs.Bitmap(P3),
  new createjs.Bitmap(P4),
  new createjs.Bitmap(P5),
  new createjs.Bitmap(P6)
];

const innerPath = [];
const innerRing = {
  upperBar: [
    { tile: tileInSV1, bg: 'inner', action: 'innerRing' },
    { tile: tileInSH3, bg: 'inner', action: 'innerRing' },
    { tile: tileInSH2, bg: 'inner', action: 'innerRing' },
    { tile: tileInSV3, bg: 'inner', action: 'innerRing' }
  ],
  rightBar: [{ tile: tileInSE, bg: 'inner', action: 'innerEntrance' }],
  lowerBar: [
    { tile: tileInSV3, bg: 'inner', action: 'innerRing' },
    { tile: tileInSH1, bg: 'inner', action: 'innerRing' },
    { tile: tileInSH4, bg: 'inner', action: 'innerRing' },
    { tile: tileInSV2, bg: 'inner', action: 'innerRing' }
  ],
  leftBar: [{ tile: tileInSV2, bg: 'inner', action: 'innerRing' }]
};

const tileOutC1 = new createjs.Bitmap(tileoutercity1);
const tileOutC2 = new createjs.Bitmap(tileoutercity2);
const tileOutC3 = new createjs.Bitmap(tileoutercity3);
const tileOutC4 = new createjs.Bitmap(tileoutercity4);
const tileOutF1 = new createjs.Bitmap(tileouterfield1);
const tileOutF2 = new createjs.Bitmap(tileouterfield2);
const tileOutF3 = new createjs.Bitmap(tileouterfield3);
const tileOutF4 = new createjs.Bitmap(tileouterfield4);
const tileOutT1 = new createjs.Bitmap(tileouterforest1);
const tileOutT2 = new createjs.Bitmap(tileouterforest2);
const tileOutT3 = new createjs.Bitmap(tileouterforest3);
const tileOutM1 = new createjs.Bitmap(tileoutermountain1);
const tileOutM2 = new createjs.Bitmap(tileoutermountain2);
const tileOutM3 = new createjs.Bitmap(tileoutermountain3);

const outerPath = [];
const outerRing = {
  upperBar: [
    { tile: tileOutC1, bg: 'outer', action: 'city' },
    { tile: tileOutF1, bg: 'outer', action: 'outerRing' },
    { tile: tileOutF2, bg: 'outer', action: 'outerRing' },
    { tile: tileOutF3, bg: 'outer', action: 'outerRing' },
    { tile: tileOutF4, bg: 'outer', action: 'outerRing' },
    { tile: tileOutC2, bg: 'outer', action: 'city' }
  ],
  rightBar: [
    { tile: tileOutM1, bg: 'outer', action: 'outerRing' },
    { tile: tileOutM2, bg: 'outer', action: 'outerRing' },
    { tile: tileOutM3, bg: 'outer', action: 'outerRing' }
  ],
  lowerBar: [
    { tile: tileOutC4, bg: 'outer', action: 'city' },
    { tile: tileOutF4, bg: 'outer', action: 'outerRing' },
    { tile: tileOutF3, bg: 'outer', action: 'outerRing' },
    { tile: tileOutF1, bg: 'outer', action: 'outerRing' },
    { tile: tileOutF2, bg: 'outer', action: 'outerRing' },
    { tile: tileOutC3, bg: 'outer', action: 'city' }
  ],
  leftBar: [
    { tile: tileOutT1, bg: 'outer', action: 'outerRing' },
    { tile: tileOutT2, bg: 'outer', action: 'outerRing' },
    { tile: tileOutT3, bg: 'outer', action: 'outerRing' }
  ]
};

const crownPath = [];

const tileTotalWidth = 200;
const tileTotalHeight = 160;
const tileWidth = 194;
const tileHeight = 154;
const tileOffsetX = -(tileTotalWidth - tileWidth) / 2;
const tileOffsetY = -(tileTotalHeight - tileHeight) / 2;

const canvasWidth = 1400;
const canvasHeight = 900;
const gameCanvasWidth = 1200;
const gameCanvasHeight = 800;

type Props = {
  lobby: LobbyState,
  history: RouterHistory,
  resetLobby: Dispatch,
  cards: Card[]
};

type State = {
  isEqPopupOpen: boolean,
  isActionPopupOpen: boolean,
  isSpecialPopupOpen: boolean,
  isNewTurnPopupOpen: boolean,
  isNewLevelPopupOpen: boolean,
  curtainHandle: any, // eslint-disable-line flowtype/no-weak-types
  activePlayerID: number,
  currentRound: number,
  currentPlayerRange: number,
  players: Player[],
  cards: {
    outer: any[], // eslint-disable-line flowtype/no-weak-types
    inner: any[] // eslint-disable-line flowtype/no-weak-types
  },
  roundTime: number,
  roundCounterInterval: null | IntervalID,
  currentAction: null | {
    card: any // eslint-disable-line flowtype/no-weak-types
  },
  isEndGame: boolean,
  currentSpecial?: string
};

class _Canvas extends Component<Props, State> {
  static contextTypes = { store: PropTypes.object.isRequired };

  state: State = {
    isEqPopupOpen: false,
    isActionPopupOpen: false,
    isSpecialPopupOpen: false,
    isNewTurnPopupOpen: false,
    isNewLevelPopupOpen: false,
    isFightPopupOpen: false,
    isRemovePlayerPopupOpen: false,
    curtainHandle: null,
    activePlayerID: 0,
    currentRound: 1,
    currentPlayerRange: 0,
    players: [],
    cards: {
      outer: [],
      inner: []
    },
    currentAction: null,
    currentSpecial: null,
    roundTime: this.props.lobby.roundTime, // eslint-disable-line react/destructuring-assignment
    roundCounterInterval: null,
    isEndGame: false
  };

  componentDidMount() {
    const { lobby } = this.props;
    const {
      store: { firestore }
    } = this.context;

    const canvasGame: any = this.canvas.current; // eslint-disable-line flowtype/no-weak-types
    const stageGame = new createjs.Stage(canvasGame);

    stageGame.mouseEventsEnabled = true;
    stageGame.snapToPixelEnabled = true;
    stageGame.enableMouseOver();
    createjs.Touch.enable(stageGame);

    const contextGame = canvasGame.getContext('2d');
    contextGame.imageSmoothingEnabled = false;

    const gameBoard = new createjs.Container();
    gameBoard.busyFlag = false;
    let curtain = new createjs.Shape();

    this.setTickRatio(stageGame, 60);
    this.buildGameboard(gameBoard);
    this.setIcons(gameBoard, lobby);
    curtain = this.buildCurtain();
    gameBoard.x = (canvasWidth - gameCanvasWidth) / 2;
    gameBoard.y = (canvasHeight - gameCanvasHeight - 6) / 2;

    stageGame.addChild(bgCanvas, gameBoard, curtain);
    this.setState(
      {
        players: lobby.players,
        isNewTurnPopupOpen: true,
        curtainHandle: curtain
      },
      () => {
        this.disableBoard();
      }
    );

    stageGame.update();
    firestore.get({ collection: 'cards' });
  }
  // TODO: check if it is necessery
  // shouldComponentUpdate(nextProps, nextState) {
  //   const { isEqPopupOpen, isActionPopupOpen, isNewTurnPopupOpen } = this.state;
  //   if (
  //     isActionPopupOpen !== nextState.isActionPopupOpen ||
  //     isEqPopupOpen !== nextState.isEqPopupOpen ||
  //     isNewTurnPopupOpen !== nextState.isNewTurnPopupOpen
  //   )
  //     return true;
  //   return false;
  // }

  componentDidUpdate(prevProps) {
    const { cards } = this.props;
    if (prevProps.cards !== cards) {
      const outer = cards.filter(
        card =>
          (card.value <= 1 &&
            (card.type === 'spirit' || card.type === 'monster')) ||
          (card.type !== 'spirit' && card.type !== 'monster')
      );

      const inner = cards.filter(
        card =>
          card.value > 1 && (card.type === 'spirit' || card.type === 'monster')
      );

      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({
        cards: {
          inner,
          outer
        }
      });
    }
  }

  // eslint-disable-next-line class-methods-use-this
  setTickRatio(stage, tickRatio) {
    createjs.Ticker.timingMode = createjs.Ticker.RAF_SYNCHED;
    createjs.Ticker.framerate = tickRatio;
    createjs.Ticker.paused = false;
    createjs.Ticker.addEventListener('tick', handleTick);
    // eslint-disable-next-line no-unused-vars
    function handleTick(event) {
      if (createjs.Ticker.paused) {
        stage.update();
        createjs.Ticker.paused = false;
      }
    }
  }

  canvas = React.createRef();

  buildGameboard(stage) {
    this.buildOuterRing(stage);
    this.buildInnerRing(stage);
    this.buildCrownRing(stage);
  }

  buildOuterRing(stage) {
    let tile;
    Object.keys(outerRing).forEach(ring => {
      for (let i = 0; i < outerRing[ring].length; i += 1) {
        let coord = { x: 0, y: 0 };
        switch (ring) {
          case 'upperBar':
            coord = { x: tileTotalWidth * i + 3, y: 3 };
            break;
          case 'rightBar':
            coord = {
              x: tileTotalWidth * (outerRing.upperBar.length - 1) + 3,
              y: tileTotalHeight * (i + 1) + 3
            };
            break;
          case 'lowerBar':
            coord = {
              x: gameCanvasWidth - tileTotalWidth * (i + 1) + 3,
              y: gameCanvasHeight - tileTotalHeight + 3
            };
            break;
          case 'leftBar':
            coord = {
              x: 3,
              y: gameCanvasHeight - tileTotalHeight * (i + 2) + 3
            };
            break;
          default:
        }
        tile = this.buildTile(
          outerRing[ring][i].tile,
          outerRing[ring][i].bg,
          outerRing[ring][i].action,
          coord
        );
        outerPath.push(tile);
        stage.addChild(tile);
      }
    });
  }

  buildInnerRing(stage) {
    let tile;
    Object.keys(innerRing).forEach(ring => {
      for (let i = 0; i < innerRing[ring].length; i += 1) {
        let coord = { x: 0, y: 0 };
        switch (ring) {
          case 'upperBar':
            coord = { x: tileTotalWidth * (i + 1) + 3, y: tileTotalHeight + 3 };
            break;
          case 'rightBar':
            coord = {
              x: tileTotalWidth * (outerRing.upperBar.length - 2) + 3,
              y: tileTotalHeight * (i + 2) + 3
            };
            break;
          case 'lowerBar':
            coord = {
              x: tileTotalWidth * (i + 1) + 3,
              y: gameCanvasHeight - 2 * tileTotalHeight + 3
            };
            break;
          case 'leftBar':
            coord = {
              x: tileTotalWidth + 3,
              y: tileTotalHeight * (i + 2) + 3
            };
            break;
          default:
        }
        tile = this.buildTile(
          innerRing[ring][i].tile,
          innerRing[ring][i].bg,
          innerRing[ring][i].action,
          coord
        );
        innerPath.push(tile);
        stage.addChild(tile);
      }
    });
  }

  buildCrownRing(stage) {
    // eslint-disable-next-line prefer-const
    let tileTrial = this.buildTile(tileFinEnd, 'crown', 'trial', {
      x: 3 * tileTotalWidth + 3,
      y: 2 * tileTotalHeight + 3
    });
    // eslint-disable-next-line prefer-const
    let tileCrown = this.buildTile(tileFinTrial, 'crown', 'end', {
      x: 2 * tileTotalWidth + 3,
      y: 2 * tileTotalHeight + 3
    });
    crownPath.push(tileTrial, tileCrown);
    stage.addChild(tileTrial, tileCrown);
  }

  // eslint-disable-next-line class-methods-use-this
  buildTile(
    tileSplash,
    type: string,
    action: string,
    offset: { x: number, y: number }
  ) {
    // eslint-disable-next-line prefer-const
    let tile = new createjs.Container();

    // eslint-disable-next-line no-param-reassign
    tileSplash.x = tileOffsetX;
    // eslint-disable-next-line no-param-reassign
    tileSplash.y = tileOffsetY;
    // eslint-disable-next-line no-param-reassign
    tileSplash.shadow = new createjs.Shadow('#000000', 0, 0, 15);

    switch (type) {
      case 'outer':
        tile.addChild(tileOut.clone());
        break;
      case 'city':
        tile.addChild(tileCit.clone());
        break;
      case 'inner':
        tile.addChild(tileIn.clone());
        break;
      case 'crown':
        tile.addChild(tileCr.clone());
        break;
      default:
        break;
    }
    tile.addChild(tileSplash);
    tile.cache(0, 0, tileWidth, tileHeight);
    tile.x = offset.x;
    tile.y = offset.y;
    tile.shadow = new createjs.Shadow('#000000', 0, 0, 10);
    tile.cursor = 'pointer';
    tile.busyFlag = false;
    tile.type = action;
    tile.animOffsets = {
      x: tile.x + tileWidth * 0.1,
      y: tile.y + tileHeight * 0.1
    };

    tile.addEventListener('mouseover', event => {
      // eslint-disable-next-line no-param-reassign
      event.target.parent.shadow = new createjs.Shadow('#ffffff', 0, 0, 10);
      createjs.Ticker.paused = true;
    });

    tile.addEventListener('mouseout', event => {
      // eslint-disable-next-line no-param-reassign
      event.target.parent.shadow = new createjs.Shadow('#000000', 0, 0, 10);
      createjs.Ticker.paused = true;
    });

    tile.addEventListener('click', event => {
      const moveCoord = this.checkMovement(event.target.parent);
      if (
        !event.target.parent.busyFlag &&
        !event.target.parent.parent.busyFlag &&
        moveCoord != null
      ) {
        // eslint-disable-next-line no-param-reassign
        event.target.parent.busyFlag = true;
        // eslint-disable-next-line no-param-reassign
        event.target.parent.parent.busyFlag = true;

        createjs.Tween.get(event.target.parent)
          .call(() => {
            createjs.Ticker.paused = true;
            this.movePlayer(event.target.parent, moveCoord);
          })
          .to(
            {
              alpha: 0.6,
              scale: 0.8,
              x: event.target.parent.animOffsets.x,
              y: event.target.parent.animOffsets.y
            },
            200
          )
          .to(
            {
              alpha: 1,
              scale: 1,
              x: event.target.parent.x,
              y: event.target.parent.y
            },
            200
          )
          .call(() => {
            // eslint-disable-next-line no-param-reassign
            event.target.parent.busyFlag = false;
            // eslint-disable-next-line no-param-reassign
            event.target.parent.parent.busyFlag = false;
            this.resolveField(event.target.parent);
          })
          .on('change', () => {
            createjs.Ticker.paused = true;
          });
      }
    });

    return tile;
  }

  // eslint-disable-next-line class-methods-use-this
  buildCurtain() {
    const curtain = new createjs.Shape();
    curtain.graphics
      .beginFill('#000000')
      .drawRect(0, 0, canvasWidth, canvasHeight);
    curtain.alpha = 0;
    curtain.cursor = 'default';
    curtain.visible = false;
    curtain.name = 'curtain';

    curtain.fadeIn = () => {
      createjs.Tween.get(curtain)
        .call(() => {
          createjs.Ticker.paused = true;
          curtain.visible = true;
        })
        .to({ alpha: 0.7 }, 200)
        .on('change', () => {
          createjs.Ticker.paused = true;
        });
    };

    curtain.fadeOut = () => {
      createjs.Tween.get(curtain)
        .call(() => {
          createjs.Ticker.paused = true;
        })
        .to({ alpha: 0, visible: false }, 200)
        .on('change', () => {
          createjs.Ticker.paused = true;
        });
    };

    return curtain;
  }

  // eslint-disable-next-line class-methods-use-this
  setIcons(stage, lobby) {
    for (let i = 0; i < lobby.players.length; i += 1) {
      let color = [0, 0, 0];
      switch (lobby.players[i].color) {
        case 'red':
          color = [255, 0, 0];
          break;
        case 'yellow':
          color = [255, 255, 0];
          break;
        case 'green':
          color = [0, 153, 51];
          break;
        case 'blue':
          color = [0, 0, 255];
          break;
        case 'pink':
          color = [255, 204, 255];
          break;
        case 'cyan':
          color = [0, 255, 255];
          break;
        case 'brown':
          color = [153, 51, 0];
          break;
        default:
      }
      playerIcons[i].filters = [
        new createjs.ColorFilter(0, 0, 0, 1, color[0], color[1], color[2])
      ];
      playerIcons[i].cache(0, 0, tileWidth, tileHeight);
      playerIcons[i].shadow = new createjs.Shadow('#000000', 0, 0, 10);
      playerIcons[i].x = stage.x;
      playerIcons[i].y = stage.y;
      stage.addChild(playerIcons[i]);
    }
  }

  checkMovement = tile => {
    const { players, activePlayerID, currentPlayerRange } = this.state;
    let targetId;
    let ring;
    switch (players[activePlayerID].position.ring) {
      case 'outer':
        if (!outerPath.includes(tile)) return null;
        targetId = this.inRange(
          outerPath,
          currentPlayerRange,
          tile,
          players[activePlayerID].position.tile
        );
        ring = 'outer';
        break;
      case 'inner':
        if (!innerPath.includes(tile)) return null;
        targetId = this.inRange(
          innerPath,
          currentPlayerRange,
          tile,
          players[activePlayerID].position.tile
        );
        ring = 'inner';
        break;
      case 'crown':
        if (!crownPath.includes(tile)) return null;
        ring = 'crown';
        break;
      default:
    }
    if (targetId < 0) return null;
    return { ring, tile: targetId };
  };

  inRange = (array, range, A, idB) => {
    const idA = array.indexOf(A);
    if (idB < idA && idB + range >= idA) return idA;
    if (idA + range >= array.length && idA - array.length < range) return idA;
    if (idB > idA && idB - range <= idA) return idA;
    return -1;
  };

  movePlayer = (tile, moveCoord) => {
    const { players, activePlayerID } = this.state;
    createjs.Tween.get(playerIcons[activePlayerID])
      .to({ x: tile.x, y: tile.y }, 400)
      .on('change', () => {
        createjs.Ticker.paused = true;
      });
    players[activePlayerID].position = moveCoord;
    this.setState({ players });
  };

  resolveField = tile => {
    const { cards } = this.state;
    if (tile.type === 'outerRing') {
      const randomIndex = Math.round(Math.random() * (cards.outer.length - 1));
      const currentAction = {
        card: cards.outer[randomIndex]
      };
      this.openAction(currentAction);
    } else if (tile.type === 'innerRing') {
      const randomIndex = Math.round(Math.random() * (cards.inner.length - 1));
      const currentAction = {
        card: cards.inner[randomIndex]
      };

      this.openAction(currentAction);
    } else if (
      tile.type === 'city' ||
      tile.type === 'innerEntrance' ||
      tile.type === 'trial' ||
      tile.type === 'end'
    ) {
      this.openSpecial(tile.type);
    } else {
      this.switchPlayer();
    }
  };

  switchPlayer = (removedPlayer: boolean) => {
    const { lobby } = this.props;
    const { currentRound } = this.state;
    let { activePlayerID } = this.state;

    activePlayerID = removedPlayer ? activePlayerID - 1 : activePlayerID;

    if (activePlayerID < lobby.players.length - 1) {
      this.setState({
        activePlayerID: activePlayerID + 1,
        isNewTurnPopupOpen: true,
        roundTime: lobby.roundTime
      });
    } else if (currentRound + 1 > lobby.roundLimit) {
      this.handleExit();
    } else {
      this.setState({
        activePlayerID: 0,
        currentRound: currentRound + 1,
        isNewTurnPopupOpen: true,
        roundTime: lobby.roundTime
      });
    }

    this.endTimer();
    this.disableBoard();
  };

  disableBoard = () => {
    const { curtainHandle } = this.state;
    if (curtainHandle) curtainHandle.fadeIn();
  };

  enableBoard = () => {
    const { curtainHandle } = this.state;
    if (curtainHandle) curtainHandle.fadeOut();
  };

  handleExit = () => {
    this.setState({ isEndGame: true });
  };

  exitGame = () => {
    const { resetLobby, history } = this.props;

    this.addPlayerToRanking(this.getWinner());
    resetLobby();
    history.push('/');
  };

  openEq = () => {
    this.setState({ isEqPopupOpen: true }, () => this.disableBoard());
  };

  openAction = currentAction => {
    this.setState({ currentAction, isActionPopupOpen: true }, () =>
      this.disableBoard()
    );
  };

  openSpecial = currentSpecial => {
    this.setState({ currentSpecial, isSpecialPopupOpen: true }, () =>
      this.disableBoard()
    );
  };

  openStartRoundPopup = () => {
    this.setState({ isNewTurnPopupOpen: true }, () => this.disableBoard());
  };

  closeEq = () => {
    this.setState({ isEqPopupOpen: false }, () => this.enableBoard());
  };

  handleBossFight = () => {
    const card = {
      asset: 'boss-dragon.jpg',
      name: 'Black dragon',
      description: 'Ancient dragon, awaken from slumber.',
      value: 7,
      type: 'boss'
    };

    this.setState({
      currentAction: { card },
      isSpecialPopupOpen: false,
      isFightPopupOpen: true
    });
  };

  handleFight = (win: boolean, isBossFight: boolean) => {
    this.setState({ isActionPopupOpen: false }, () => {
      const {
        currentAction: { card },
        activePlayerID,
        players
      } = this.state;

      const player = players[activePlayerID];
      if (win) {
        const exp = (player.exp += card.value);
        if (exp >= EXPERIENCE_CAP) {
          player.exp -= EXPERIENCE_CAP;
          this.setState({ players });
          this.setState({
            isNewLevelPopupOpen: true,
            isFightPopupOpen: false
          });
        } else {
          this.setState({ players, isFightPopupOpen: false }, () => {
            if (isBossFight) this.handleMovePlayerToCrown();
            this.switchPlayer();
          });
        }
      } else {
        player.character = { ...player.character };
        player.character.hp -= 1;
        if (player.character.hp === 0) {
          this.setState({
            isFightPopupOpen: false,
            isRemovePlayerPopupOpen: true
          });
        } else {
          this.setState({ players, isFightPopupOpen: false });
          if (isBossFight) {
            this.handleMovePlayerToCity();
          } else this.switchPlayer();
        }
      }
    });
  };

  // TODO: add win if last stays
  removeActivePlayer = () => {
    const { players, activePlayerID } = this.state;
    this.setState({
      players: [
        ...players.slice(0, activePlayerID),
        ...players.slice(activePlayerID + 1)
      ],
      isEndGame: players.length === 1
    });
  };

  showBattle = () => {
    this.setState({ isFightPopupOpen: true, isActionPopupOpen: false }, () =>
      this.disableBoard()
    );
  };

  closeNewLevel = action => {
    const { activePlayerID, players } = this.state;
    players[activePlayerID].character = {
      ...players[activePlayerID].character
    };
    switch (action) {
      case STAT_TYPE.strength:
        players[activePlayerID].character.strength += 1;
        break;

      case STAT_TYPE.magic:
        players[activePlayerID].character.magic += 1;
        break;

      default:
        break;
    }

    this.setState({ players, isNewLevelPopupOpen: false }, () =>
      this.switchPlayer()
    );
  };

  closeRemovePopup = () => {
    this.setState({ isRemovePlayerPopupOpen: false }, () => {
      this.removeActivePlayer();
      this.switchPlayer();
    });
  };

  closeSpecial = () => {
    this.setState({ isSpecialPopupOpen: false }, () => {
      this.switchPlayer();
    });
  };

  closeStartRoundPopup = (roll: number) => {
    const { players, activePlayerID } = this.state;
    // eslint-disable-next-line no-unused-vars
    const activePlayer = players[activePlayerID];
    this.setState(
      { isNewTurnPopupOpen: false, currentPlayerRange: roll },
      () => {
        this.enableBoard();
        this.startTimer();
      }
    );
  };

  startTimer = () => {
    const interval = setInterval(() => {
      const { roundTime } = this.state;
      if (roundTime < 1) {
        this.endTimer();
        this.switchPlayer();
      } else {
        this.setState({ roundTime: roundTime - 1 });
      }
    }, 1000);
    this.setState({ roundCounterInterval: interval });
  };

  endTimer = () => {
    const { roundCounterInterval } = this.state;

    if (roundCounterInterval) {
      clearInterval(roundCounterInterval);
    }
  };

  handleEquip = card => {
    const { players, activePlayerID } = this.state;
    const activePlayer = players[activePlayerID];
    if (!activePlayer.equipment) {
      activePlayer.equipment = [];
    }

    activePlayer.equipment.push(card);

    // duplicate object to avoid assignments to readonly object
    activePlayer.character = { ...activePlayer.character };

    if (card.type === 'weapon') {
      activePlayer.character.strength += card.value;
    }

    if (card.type === 'spell') {
      activePlayer.character.magic += card.value;
    }

    if (card.type === 'gold') {
      activePlayer.character.gold += card.value;
    }
    this.setState({ players, isActionPopupOpen: false }, () =>
      this.switchPlayer()
    );
  };

  handlePlayerHeal = () => {
    const { players, activePlayerID } = this.state;
    const activePlayer = players[activePlayerID];
    activePlayer.character = { ...activePlayer.character };
    if (activePlayer.character.gold >= 1) {
      activePlayer.character.gold -= 1;
      activePlayer.character.hp += 1;
    }
    this.setState({ players, isSpecialPopupOpen: false }, () => {
      this.switchPlayer();
    });
  };

  handleMovePlayerToInner = () => {
    const { players, activePlayerID } = this.state;
    const activePlayer = players[activePlayerID];
    activePlayer.character = { ...activePlayer.character };
    if (activePlayer.character.gold >= 5) {
      activePlayer.character.gold -= 5;
      this.movePlayer(innerPath[4], { ring: 'inner', tile: 4 });
    }
    this.setState({ players, isSpecialPopupOpen: false }, () => {
      this.switchPlayer();
    });
  };

  handleMovePlayerToCity = () => {
    switch (Math.floor(Math.random() * 4)) {
      case 0:
        this.movePlayer(outerPath[0], { ring: 'outer', tile: 0 });
        break;
      case 1:
        this.movePlayer(outerPath[5], { ring: 'outer', tile: 5 });
        break;
      case 2:
        this.movePlayer(outerPath[9], { ring: 'outer', tile: 9 });
        break;
      case 3:
        this.movePlayer(outerPath[14], { ring: 'outer', tile: 14 });
        break;
      default:
    }
    this.setState({ isSpecialPopupOpen: false }, () => {
      this.switchPlayer();
    });
  };

  handleMovePlayerToBoss = () => {
    this.movePlayer(crownPath[0], { ring: 'crown', tile: 0 });
    this.setState({ isSpecialPopupOpen: false }, () => {
      this.resolveField(crownPath[0]);
    });
  };

  handleMovePlayerToCrown = () => {
    this.movePlayer(crownPath[1], { ring: 'crown', tile: 1 });
    this.setState({ isSpecialPopupOpen: false }, () => {
      this.resolveField(crownPath[1]);
    });
  };

  getWinner = (): Player => {
    const { players } = this.state;

    if (players.filter(player => player.character.hp > 0).length === 1) {
      // last one alive, he is the winner
      return players.filter(player => player.character.hp > 0)[0];
    }

    // end of the rounds or exit clicked, get winner with most gold
    const sortedByGold = [...players].sort(
      (a, b) => b.character.gold - a.character.gold
    );
    return sortedByGold[0];
  };

  addPlayerToRanking = (player: Player) => {
    const {
      store: { firestore }
    } = this.context;
    firestore.add(
      { collection: 'ranking' },
      {
        character: player.character.name,
        color: player.color,
        name: player.name,
        score:
          player.character.gold +
          player.character.hp +
          player.character.magic +
          player.character.strength
      }
    );
  };

  render() {
    const {
      isEqPopupOpen,
      isActionPopupOpen,
      isSpecialPopupOpen,
      isNewTurnPopupOpen,
      isNewLevelPopupOpen,
      isFightPopupOpen,
      isRemovePlayerPopupOpen,
      activePlayerID,
      currentPlayerRange,
      players,
      currentAction,
      currentSpecial,
      currentRound,
      roundTime,
      isEndGame
    } = this.state;
    const {
      lobby: { roundLimit }
    } = this.props;

    return (
      <React.Fragment>
        <div className={styles.upperBar}>
          <TimerBar
            round={currentRound}
            roundTime={roundTime}
            roundLimit={roundLimit}
          />
          <PlayerBar players={players} />
        </div>
        <div className={styles.mainScreen}>
          <canvas
            ref={this.canvas}
            width="1400px"
            height="900px"
            className={
              isEqPopupOpen || isActionPopupOpen || isSpecialPopupOpen
                ? 'blur'
                : ''
            }
          />
          <div className={styles.sideBar}>
            {players[activePlayerID] && (
              <PlayerCard
                range={currentPlayerRange}
                player={players[activePlayerID]}
              />
            )}
            <button
              className={styles.buttonSpecial}
              type="button"
              onClick={this.openEq}
            >
              Show equipment
            </button>
            <button
              className={styles.buttonSpecial}
              type="button"
              onClick={this.handleExit}
            >
              Exit game
            </button>

            {isEqPopupOpen && (
              <EquipmentPopup
                playerItems={players[activePlayerID].equipment || []}
                handleClose={this.closeEq}
              />
            )}
            {isActionPopupOpen && (
              <ActionPopup
                handleFight={this.showBattle}
                handleEquip={this.handleEquip}
                action={currentAction}
              />
            )}
            {isSpecialPopupOpen && (
              <SpecialPopup
                handleClose={this.closeSpecial}
                handleMovePlayerToInner={this.handleMovePlayerToInner}
                handlePlayerHeal={this.handlePlayerHeal}
                handleMovePlayerToBoss={this.handleMovePlayerToBoss}
                handleBossFight={this.handleBossFight}
                handleExit={this.handleExit}
                specialType={currentSpecial}
              />
            )}
            {isNewTurnPopupOpen && (
              <StartRoundPopup
                handleClose={this.closeStartRoundPopup}
                player={players[activePlayerID]}
              />
            )}
            {isNewLevelPopupOpen && (
              <NewLevelPopup raise={this.closeNewLevel} />
            )}
            {isFightPopupOpen && (
              <FightPopup
                enemy={currentAction.card}
                player={players[activePlayerID]}
                handleClose={this.handleFight}
              />
            )}
            {isRemovePlayerPopupOpen && (
              <RemovePlayerPopup
                player={players[activePlayerID]}
                handleClose={this.closeRemovePopup}
              />
            )}
            {isEndGame && (
              <EndGamePopup
                player={this.getWinner()}
                handleClose={this.exitGame}
              />
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

const mapState = ({ lobby, firestore }) => ({
  lobby,
  cards: firestore.ordered.cards
});

const mapDispatch = dispatch => ({
  resetLobby: () => dispatch(resetLobbyPlayers())
});

export const Canvas = withRouter(
  connect(
    mapState,
    mapDispatch
  )(_Canvas)
);
